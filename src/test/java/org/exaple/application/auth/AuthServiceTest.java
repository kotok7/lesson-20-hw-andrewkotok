package org.exaple.application.auth;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {


    public static final String LOGIN = "vasia";
    public static final String PASSWORD = "0000";
    public static final User TEST_USER = new User(LOGIN, PASSWORD);
    public static final User TEST_USER_INCORRECT = new User(LOGIN, "wrongPass");

    @Mock
    private UserStorage userStorage;
    @InjectMocks
    private AuthService authService;

    @Test
    void shouldLogin() {
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.of(TEST_USER));
        assertFalse(authService.isAuth());
        authService.login(TEST_USER);
        assertTrue(authService.isAuth());
        assertEquals(authService.current().get(), TEST_USER);
    }

    @Test
    void shouldNotLoginIfUserNotFound() {
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.empty());
        assertFalse(authService.isAuth());
        assertThrows(IllegalArgumentException.class,
                () -> authService.login(TEST_USER),
                "Login or password is incorrect");
        assertFalse(authService.isAuth());
    }

    @Test
    void shouldNotLoginIfIncorrectPassword() {
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.of(TEST_USER));
        assertFalse(authService.isAuth());
        assertThrows(IllegalArgumentException.class,
                () -> authService.login(TEST_USER_INCORRECT),
                "Login or password is incorrect");
        assertFalse(authService.isAuth());
    }

    @Test
    void shouldRegister(){
    when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.empty());
    authService.register(TEST_USER);
    verify(userStorage).save(TEST_USER);
    }

    @Test
    void shouldNotRegisterWhenUserExists(){
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.of(TEST_USER));
        assertThrows(IllegalArgumentException.class,
                ()->authService.register(TEST_USER),
                "User already exists");
        verify(userStorage,never()).save(any());
    }

    @Test
    void shouldNotRegisterWhenUserLoginOrPassIsIncorrect(){
        User user = new User(null,null);
        assertThrows(IllegalArgumentException.class,
                ()->authService.register(user),
                "User login or password is incorrect");
        verify(userStorage,never()).findByLogin(any());
        verify(userStorage,never()).save(any());
    }
}