package org.exaple.application.infrastructure.menu;

public class ExitMenuItem implements MenuItem{


    @Override
    public String getName() {
        return "Exit\n";
    }

    @Override
    public void run() {

    }

    @Override
    public boolean isFinal() {
        return true;
    }

}
