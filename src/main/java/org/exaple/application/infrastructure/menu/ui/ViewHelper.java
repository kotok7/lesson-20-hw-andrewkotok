package org.exaple.application.infrastructure.menu.ui;

import lombok.RequiredArgsConstructor;

import java.util.Scanner;
@RequiredArgsConstructor
public class ViewHelper {
    private final Scanner scanner;

    public String readString(String message) {
        System.out.print(message);
        return scanner.nextLine();
    }

    public int readInt (String message) {
        System.out.print(message);
        int value = scanner.nextInt();
        scanner.nextLine();
        return value;
    }

    public void showError(String error) {
        System.err.println(error);
    }
}
