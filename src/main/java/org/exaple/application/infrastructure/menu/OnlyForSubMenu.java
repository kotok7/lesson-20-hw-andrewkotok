package org.exaple.application.infrastructure.menu;

import org.exaple.application.auth.AuthService;

public class OnlyForSubMenu extends SubMenu{
    private final AuthService authService;

    public OnlyForSubMenu(String name, AuthService authService, Menu menu) {
        super(name,menu);
        this.authService = authService;
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
