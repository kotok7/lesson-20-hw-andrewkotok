package org.exaple.application.infrastructure.menu;

public interface MenuItem {
    String getName();
    void run();
    default boolean isFinal(){
        return false;
    }

    default boolean isVisible(){
        return true;
    }
}
