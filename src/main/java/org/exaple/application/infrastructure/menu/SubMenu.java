package org.exaple.application.infrastructure.menu;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SubMenu implements MenuItem {
    private final String name;
    private final Menu menu;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void run() {
        menu.run();
    }

}
