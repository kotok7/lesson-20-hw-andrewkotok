package org.exaple.application.ticket;

public enum TicketStatus {
    NEW, IN_PROGRESS, DONE;
}
