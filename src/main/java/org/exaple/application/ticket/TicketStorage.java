package org.exaple.application.ticket;

import java.util.Optional;
import java.util.stream.Stream;

public interface TicketStorage {
    Stream<Ticket> getAll();
    Optional<Ticket> findByID(String ticketID);
    void save (Ticket ticket);

//    using only in InMemoryTicketStorage
    Optional<Ticket> findByUserName(String userName);
    Optional<Ticket> findByCreatorName(String userName);
    Optional<Ticket> showByStatus(TicketStatus status);



}
