package org.exaple.application.ticket;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Ticket {
    private final String ticketID;
    private final String ticketProblem;
    private final String ticketMail;
    private final String ticketCreator;
    private String ticketOperator;
//    private final LocalDate date; using with InMemoryTicketStorage
    private final String date;
    private TicketStatus status;

    public TicketStatus setStatus(TicketStatus status) {
        this.status = status;
        return status;
    }

    public void setOperator(String newOperator) {
        this.ticketOperator = newOperator;
    }

    @Override
    public String toString() {
        return "[" +
                "ticketID: " + ticketID +
                ", ticketProblem: " + ticketProblem +
                ", ticketMail: " + ticketMail +
                ", ticketCreator: " + ticketCreator +
                ", ticketOperator: " + ticketOperator +
                ", date: " + date +
                ", status: " + status +
                ']'+"\n";
    }
}
