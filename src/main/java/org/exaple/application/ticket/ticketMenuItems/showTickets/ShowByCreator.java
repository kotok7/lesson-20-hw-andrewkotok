package org.exaple.application.ticket.ticketMenuItems.showTickets;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.ticket.TicketStorage;

import java.util.Scanner;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ShowByCreator implements MenuItem {
    private final TicketStorage ticketStorage;
    private final Scanner scanner;


    @Override
    public String getName() {
        return "Search ticket by ticket Creator\n";
    }

    @Override
    public void run() {
        System.out.println("Enter Creator name: ");
        String creator = scanner.nextLine();
        if(ticketStorage.getAll().anyMatch(t -> t.toString().equals(creator))){
            System.out.println("No Ticket Creator with such name!");
        } else System.out.println(ticketStorage.getAll().filter(t->t.getTicketCreator()
                .equals(creator)).collect(Collectors.toList()));
    }

    @Override
    public boolean isVisible() {
        return ticketStorage.getAll().findAny().isPresent();
    }
}
