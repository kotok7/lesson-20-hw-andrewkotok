package org.exaple.application.ticket.ticketMenuItems.changeTickets;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.ticket.TicketStatus;
import org.exaple.application.ticket.TicketStorage;

import java.util.Scanner;

@RequiredArgsConstructor
public class ChangeTicketStatus implements MenuItem {
    private final TicketStorage ticketStorage;
    private final Scanner scanner;

    @Override
    public String getName() {
        return "Change ticket status\n";
    }

    @Override
    public void run() {
        System.out.println("Enter ticket ID you you want to change: ");
        String ticketID = scanner.nextLine();
        if (ticketStorage.getAll().anyMatch(t -> t.toString().equals(ticketID))) {
            System.out.println("No ticket with such ID!");
        } else {
            System.out.println(ticketStorage.findByID(ticketID));
            System.out.println("What ticket Status you want to set?");
            System.out.println("You can enter only: 'NEW', 'IN_PROGRESS' and 'DONE'!");
            changeStatus(ticketID);
        }
    }

    private void changeStatus(String ticketID) {
        String newStatus = scanner.nextLine();
        if (newStatus.equals("NEW") || newStatus.equals("IN_PROGRESS") || newStatus.equals("DONE")) {
           ticketStorage.findByID(ticketID).get().setStatus(TicketStatus.valueOf(newStatus));
        }
    }

}
