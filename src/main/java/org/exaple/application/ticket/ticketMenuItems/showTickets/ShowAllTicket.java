package org.exaple.application.ticket.ticketMenuItems.showTickets;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.ticket.TicketStorage;

import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ShowAllTicket implements MenuItem {
    private final TicketStorage ticketStorage;



    @Override
    public String getName() {
        return "Show all tickets\n";
    }

    @Override
    public void run() {
            System.out.print(ticketStorage.getAll().collect(Collectors.toList()));
    }

    @Override
    public boolean isVisible() {
        return ticketStorage.getAll().findAny().isPresent();
    }
}
