package org.exaple.application.ticket.ticketMenuItems;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.ticket.Ticket;
import org.exaple.application.ticket.TicketService;
import org.exaple.application.ticket.TicketView;

@RequiredArgsConstructor
public class AddNewTicket implements MenuItem {
    private final TicketService ticketService;
    private final TicketView ticketView;

    @Override
    public String getName() {
        return "Create new ticket\n";
    }

    @Override
    public void run() {
        Ticket ticket = ticketView.writeTicket();
        try {
            ticketService.addNewTicket(ticket);
        } catch (IllegalArgumentException exception) {
            ticketView.showError(exception.getMessage());
        }
    }

}
