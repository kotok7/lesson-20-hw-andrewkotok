package org.exaple.application.ticket.ticketMenuItems.showTickets;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.ticket.TicketStorage;

import java.util.Scanner;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ShowByID implements MenuItem {
    private final TicketStorage ticketStorage;
    private final Scanner scanner;


    @Override
    public String getName() {
        return "Search ticket by ID\n";
    }

    @Override
    public void run() {
        System.out.println("Enter ticket ID you are looking for: ");
        String ticketID = scanner.nextLine();
        if(ticketStorage.getAll().findFirst().equals(ticketID)){
            System.out.println("No ticket with such ID!");
        } else System.out.println(ticketStorage.getAll().filter(t->t.getTicketID()
                .equals(ticketID)).collect(Collectors.toList()));
    }

    @Override
    public boolean isVisible() {
        return ticketStorage.getAll().findAny().isPresent();
    }
}
