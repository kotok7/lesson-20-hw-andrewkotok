package org.exaple.application.ticket.ticketMenuItems.showTickets;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.ticket.TicketStorage;

import java.util.Scanner;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ShowByOperator implements MenuItem {
    private final TicketStorage ticketStorage;
    private final Scanner scanner;


    @Override
    public String getName() {
        return "Search ticket by Operator\n";
    }

    @Override
    public void run() {
        System.out.println("Enter Operator name: ");
        String operator = scanner.nextLine();
        if(ticketStorage.getAll().anyMatch(t -> t.toString().equals(operator))){
            System.out.println("No Operator with such name!");
        } else System.out.println(ticketStorage.getAll().filter(t->t.getTicketOperator().equals(operator)).collect(Collectors.toList()));
    }

    @Override
    public boolean isVisible() {
        return ticketStorage.getAll().findAny().isPresent();
    }
}
