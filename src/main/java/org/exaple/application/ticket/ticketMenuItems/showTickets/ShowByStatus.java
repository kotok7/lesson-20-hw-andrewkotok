package org.exaple.application.ticket.ticketMenuItems.showTickets;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.ticket.TicketStatus;
import org.exaple.application.ticket.TicketStorage;

import java.util.Scanner;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ShowByStatus implements MenuItem {
    private final TicketStorage ticketStorage;
    private final Scanner scanner;


    @Override
    public String getName() {
        return "Show by Ticket Status\n";
    }

    @Override
    public void run() {
        System.out.println("Enter ticket status to see all such tickets: ");
        TicketStatus status = TicketStatus.valueOf(scanner.nextLine());
        if(ticketStorage.getAll().anyMatch(t -> t.toString().equals(status))){
            System.out.println("No ticket with such ID!");
        } else System.out.println(ticketStorage.getAll().filter(t->t.getStatus()
                .equals(status)).collect(Collectors.toList()));
    }

    @Override
    public boolean isVisible() {
        return ticketStorage.getAll().findAny().isPresent();
    }
}
