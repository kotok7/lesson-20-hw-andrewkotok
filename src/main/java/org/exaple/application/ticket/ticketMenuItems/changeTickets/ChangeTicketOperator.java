package org.exaple.application.ticket.ticketMenuItems.changeTickets;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.ticket.TicketStorage;

import java.util.Scanner;

@RequiredArgsConstructor
public class ChangeTicketOperator implements MenuItem {
    private final TicketStorage ticketStorage;
    private final Scanner scanner;

    @Override
    public String getName() {
        return "Change ticket Operator\n";
    }

    @Override
    public void run() {
        System.out.println("Enter ticket ID you you want to change: ");
        String ticketID = scanner.nextLine();
        if (ticketStorage.getAll().anyMatch(t -> t.toString().equals(ticketID))) {
            System.out.println("No ticket with such ID!");
        } else {
            System.out.println(ticketStorage.findByID(ticketID));
            System.out.println("Enter login of new Operator: ");
            changeOperator(ticketID);
        }
    }

    private void changeOperator(String ticketID) {
        String newOperator = scanner.nextLine();
            ticketStorage.findByID(ticketID).get().setOperator(newOperator);
    }
}
