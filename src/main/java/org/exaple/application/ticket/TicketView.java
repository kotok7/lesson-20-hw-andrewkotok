package org.exaple.application.ticket;

import lombok.RequiredArgsConstructor;
import org.exaple.application.infrastructure.menu.ui.ViewHelper;

@RequiredArgsConstructor
public class TicketView {
    private final ViewHelper viewHelper;

    public Ticket writeTicket() {
        String ticketID = viewHelper.readString("Enter ID of new ticket: ").toUpperCase();
        String ticketProblem = viewHelper.readString("Whats the problem: ").toUpperCase();
        String ticketMail = viewHelper.readString("Enter client's email: ").toUpperCase();
        String ticketCreator = viewHelper.readString("Who created this ticket: ");
        String ticketOperator = viewHelper.readString("Who will be fixing it: ");
        String date = viewHelper.readString("Date of Ticket creation: ");
//        LocalDate date = LocalDate.now(); Using with InMemoryTicketStorage
        TicketStatus status = TicketStatus.NEW;
        return new Ticket(ticketID, ticketProblem, ticketMail, ticketCreator, ticketOperator, date, status);
    }
    public void showError (String error){
        viewHelper.showError(error);
    }

}
