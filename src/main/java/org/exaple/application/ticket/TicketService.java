package org.exaple.application.ticket;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TicketService {
    private final TicketStorage ticketStorage;

    public void addNewTicket(Ticket ticket) {
        if (isNullOrEmpty(ticket.getTicketID())
        ) {
            throw new IllegalArgumentException("Ticket ID is incorrect");
        }
        boolean exists = ticketStorage.findByID(ticket.getTicketID()).isPresent();
        if (exists) throw new IllegalArgumentException("Ticket with such ID already exists");
        ticketStorage.save(ticket);
    }

    private static boolean isNullOrEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }
}
