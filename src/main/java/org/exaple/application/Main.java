package org.exaple.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.exaple.application.auth.AuthService;
import org.exaple.application.auth.UserStorage;
import org.exaple.application.auth.UserView;
import org.exaple.application.auth.menu.LoginMenuItem;
import org.exaple.application.auth.menu.LogoutMenuItem;
import org.exaple.application.auth.menu.RegisterMenuItem;
import org.exaple.application.auth.storage.forTickets.TextFileTicketStorage;
import org.exaple.application.auth.storage.forUsers.TextFileUserStorage;
import org.exaple.application.infrastructure.menu.ExitMenuItem;
import org.exaple.application.infrastructure.menu.Menu;
import org.exaple.application.infrastructure.menu.MenuItem;
import org.exaple.application.infrastructure.menu.OnlyForSubMenu;
import org.exaple.application.infrastructure.menu.ui.ViewHelper;
import org.exaple.application.ticket.TicketService;
import org.exaple.application.ticket.TicketStorage;
import org.exaple.application.ticket.TicketView;
import org.exaple.application.ticket.ticketMenuItems.AddNewTicket;
import org.exaple.application.ticket.ticketMenuItems.changeTickets.ChangeTicketOperator;
import org.exaple.application.ticket.ticketMenuItems.changeTickets.ChangeTicketStatus;
import org.exaple.application.ticket.ticketMenuItems.showTickets.*;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ObjectMapper objectMapper = new ObjectMapper();

        ViewHelper viewHelper = new ViewHelper(scanner);
        UserView userView = new UserView(viewHelper);
//        UserStorage UserStorage = new InMemoryUserStorage();
//        UserStorage UserStorage = new JsonFileUserStorage("user.json",objectMapper);
        UserStorage UserStorage = new TextFileUserStorage("./users.txt");
        AuthService authService = new AuthService(UserStorage);

//        TicketStorage ticketStorage = new InMemoryTicketStorage();
//        TicketStorage ticketStorage = new JsonFileTicketStorage("./tickets.json",objectMapper);
        TicketStorage ticketStorage = new TextFileTicketStorage("./tickets.txt");

        TicketService ticketService = new TicketService(ticketStorage);
        TicketView ticketView = new TicketView(viewHelper);
        List<MenuItem> items = List.of(
                new LoginMenuItem(authService, userView),
                new RegisterMenuItem(authService, userView),
                new LogoutMenuItem(authService),
                new OnlyForSubMenu("Tickets\n", authService, new Menu(
                        List.of(
                                new AddNewTicket(ticketService, ticketView),
                                new OnlyForSubMenu("Show tickets\n", authService, new Menu(
                                        List.of(
                                                new ShowAllTicket(ticketStorage),
                                                new ShowByID(ticketStorage, scanner),
                                                new ShowByOperator(ticketStorage, scanner),
                                                new ShowByCreator(ticketStorage, scanner),
                                                new ShowByStatus(ticketStorage, scanner),
                                                new ExitMenuItem()
                                        ),
                                        scanner
                                )),
                                new OnlyForSubMenu("Change tickets\n", authService, new Menu(
                                        List.of(
                                                new ChangeTicketStatus(ticketStorage, scanner),
                                                new ChangeTicketOperator(ticketStorage, scanner),
                                                new ExitMenuItem()
                                        ),
                                        scanner
                                )),
                                new ExitMenuItem()
                        ),
                        scanner
                )),
                new ExitMenuItem()
        );
        Menu menu = new Menu(items, scanner);
        menu.run();
    }
}
