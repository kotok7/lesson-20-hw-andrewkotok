package org.exaple.application.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String login;
    private String password;

    public boolean hasPassword(String password) {
        return Objects.equals(this.password,password);
    }
}
