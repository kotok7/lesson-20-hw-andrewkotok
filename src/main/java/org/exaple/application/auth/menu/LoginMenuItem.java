package org.exaple.application.auth.menu;

import lombok.RequiredArgsConstructor;
import org.exaple.application.auth.AuthService;
import org.exaple.application.auth.User;
import org.exaple.application.auth.UserView;
import org.exaple.application.infrastructure.menu.MenuItem;

@RequiredArgsConstructor
public class LoginMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Login\n";
    }

    @Override
    public void run() {
        User user = userView.readUser();
        try {
            authService.login(user);
        } catch (IllegalArgumentException exception) {
            userView.showError(exception.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return !authService.isAuth();
    }
}
