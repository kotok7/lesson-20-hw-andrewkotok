package org.exaple.application.auth.menu;

import lombok.RequiredArgsConstructor;
import org.exaple.application.auth.AuthService;
import org.exaple.application.infrastructure.menu.MenuItem;

@RequiredArgsConstructor
public class LogoutMenuItem implements MenuItem {
    private final AuthService authService;

    @Override
    public String getName() {
        return "Logout\n";
    }

    @Override
    public void run() {
        authService.exit();
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
