package org.exaple.application.auth.storage.forUsers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.exaple.application.auth.User;
import org.exaple.application.auth.UserStorage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class JsonFileUserStorage implements UserStorage {
    private final String filePath;
    private final ObjectMapper objectMapper;

    @Override
    public Stream<User> getAll() {
        try {
            return objectMapper.readValue(new File(filePath), new TypeReference<List<User>>() {}).stream();
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return getAll()
                .filter(u->u.getLogin().equals(login))
                .findFirst();
    }

    @Override
    public void save(User user) {
        List<User> users = getAll().collect(Collectors.toList());
        users.add(user);
        try {
            objectMapper.writeValue(new File(filePath),users);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
