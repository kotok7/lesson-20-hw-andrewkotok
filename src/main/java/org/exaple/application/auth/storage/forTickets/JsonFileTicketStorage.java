package org.exaple.application.auth.storage.forTickets;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.exaple.application.ticket.Ticket;
import org.exaple.application.ticket.TicketStatus;
import org.exaple.application.ticket.TicketStorage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
@RequiredArgsConstructor
public class JsonFileTicketStorage implements TicketStorage {
    private final String filePath;
    private final ObjectMapper objectMapper;


    @Override
    public Stream<Ticket> getAll() {
        try {
            return objectMapper.readValue(new File(filePath),new TypeReference<List<Ticket>>() {}).stream();
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    @Override
    public Optional<Ticket> findByID(String ticketID) {
        return getAll()
                .filter(u -> u.getTicketID().equals(ticketID))
                .findFirst();
    }

    @Override
    public Optional<Ticket> findByUserName(String operator) {
        return null;
    }

    @Override
    public Optional<Ticket> findByCreatorName(String creator) {
        return null;
    }

    @Override
    public Optional<Ticket> showByStatus(TicketStatus status) {
        return null;
    }

    @Override
    public void save(Ticket ticket) {
        List<Ticket> tickets = getAll().collect(Collectors.toList());
        tickets.add(ticket);
        try {
            objectMapper.writeValue(new File(filePath),tickets);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
