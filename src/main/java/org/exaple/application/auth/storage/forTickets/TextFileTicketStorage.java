package org.exaple.application.auth.storage.forTickets;

import lombok.RequiredArgsConstructor;
import org.exaple.application.ticket.Ticket;
import org.exaple.application.ticket.TicketStatus;
import org.exaple.application.ticket.TicketStorage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class TextFileTicketStorage implements TicketStorage {
    private final String filePath;


    @Override
    public Stream<Ticket> getAll() {
        try {
            return Files.lines(Path.of(filePath))
                    .filter(line -> !line.trim().startsWith("#"))
                    .filter(line -> !line.trim().isEmpty())
                    .map(this::extractTicket);
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    private Ticket extractTicket(String s) {
        String[] elements = s.split("\\s\\|\\s");
        return new Ticket(elements[0], elements[1], elements[2], elements[3], elements[4], elements[5], TicketStatus.NEW);
    }

    @Override
    public Optional<Ticket> findByID(String ticketID) {
        return getAll()
                .filter(u -> u.getTicketID().equals(ticketID))
                .findFirst();
    }

    @Override
    public void save(Ticket ticket) {
        getAll().map(t -> {
                    if (t.getTicketID().equals(ticket.getTicketID())) {
                        return ticket;
                    } else {
                        return t;
                    }
                }).map(this::toLine)
                .forEach(ticketLine -> {
                    try {
                        Files.write(Path.of(filePath + "_tmp"), ticketLine);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        Path oldFile = Path.of(filePath);
        try {
            Files.delete(oldFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Files.move(Path.of(filePath + "_tmp"), oldFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Files.write(oldFile, toLine(ticket), StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Iterable<String> toLine(Ticket ticket) {
        return List.of(ticket.getTicketID() + " | " + ticket.getTicketProblem() +
                " | " + ticket.getTicketMail() + " | " + ticket.getTicketCreator() +
                " | " + ticket.getTicketOperator() + " | " + ticket.getDate() + " | " + TicketStatus.NEW);
    }

    @Override
    public Optional<Ticket> findByUserName(String operator) {
        return null;
    }

    @Override
    public Optional<Ticket> findByCreatorName(String creator) {
        return null;
    }

    @Override
    public Optional<Ticket> showByStatus(TicketStatus status) {
        return null;
    }

}
