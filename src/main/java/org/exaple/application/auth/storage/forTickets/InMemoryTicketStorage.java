package org.exaple.application.auth.storage.forTickets;

import org.exaple.application.ticket.Ticket;
import org.exaple.application.ticket.TicketStatus;
import org.exaple.application.ticket.TicketStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class InMemoryTicketStorage implements TicketStorage {
    List<Ticket> ticketList = new ArrayList<>();

    @Override
    public Stream<Ticket> getAll() {
        return ticketList.stream();
    }

    @Override
    public Optional<Ticket> findByID(String ticketID) {
        return ticketList.stream()
                .filter(u -> u.getTicketID().equals(ticketID))
                .findFirst();
    }

    @Override
    public Optional<Ticket> findByUserName(String operator) {
        return ticketList.stream()
                .filter(u -> u.getTicketOperator().equals(operator))
                .findFirst();
    }

    @Override
    public Optional<Ticket> findByCreatorName(String creator) {
        return ticketList.stream()
                .filter(u -> u.getTicketCreator().equals(creator))
                .findFirst();
    }

    @Override
    public Optional<Ticket> showByStatus(TicketStatus status) {
        return ticketList.stream()
                .filter(u -> u.getStatus().equals(TicketStatus.NEW))
                .findFirst();
    }

    @Override
    public void save(Ticket ticket) {
        ticketList.add(ticket);
    }
}
